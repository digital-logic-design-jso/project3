module ALU1 (input signed [7:0] A, B , input [2:0] n, s, input Ci, output logic [7:0] w, output o, z, neg, gt, eq ,output logic Co); 
  always @(s,A,B,Ci,n) begin
    w = 8'b0;
    Co = 1'b0;
    case(s)
      0 : {Co,w} = A + B + Ci;
      1 : {Co,w} = A + B;
      2 : if( A > B )
            w = A;
        else w = B;
      3 : {Co,w} =  A + (A <<< n);
      4 : {Co,w} =  A + (A >>> n);
      5 : if ( A[7] == 1 )
            w = ~A + 8'b00000001;
        else w = A;
      6 : {Co,w} = A + (B << 1);
      7 : w = A & B;
      default : w = 8'bxxxxxxxx;
    endcase
  end
  assign o = (~A[7] & ~B[7] & w[7]) | (A[7] & B[7] & ~w[7]);
  assign z = ~| w;
  assign neg = w[7];
  assign {gt,eq} = (A > B) ? 2'b10 : (A == B) ? 2'b01 : 2'bzz;
endmodule