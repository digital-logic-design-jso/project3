module P1TB ();
  reg signed [7:0]A,B;
  reg [2:0]S , N;
  reg Ci;
  wire [7:0] WW;
  wire OO, ZZ, NEG, EQ, GT, CO;
  ALU1 alu(A,B,N,S,Ci,WW,OO, ZZ, NEG, EQ, GT, CO);
  initial begin
    A=8'b00100101; //37
    B=8'b10101010; //-86
    N=3'b010;
    Ci=1;
    S=3'b000;
    #30;
    S=3'b001;
    #30;
    S=3'b010; 
    #30;
    S=3'b011;
    #30;
    S=3'b100;
    #30;
    S=3'b101;
    #30;
    S=3'b0110;
    #30;
    S=3'b111;
    
    #100;
    $stop;
  end
  
  
endmodule

